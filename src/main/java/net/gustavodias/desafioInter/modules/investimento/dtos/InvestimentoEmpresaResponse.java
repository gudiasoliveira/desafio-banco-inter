package net.gustavodias.desafioInter.modules.investimento.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoResponse;

import static net.gustavodias.desafioInter.modules.commons.utils.NumberUtils.roundCurrency;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Schema(description = "Uma unidade de investimentos correspondente a uma única ação com sua quantidade comprada")
public class InvestimentoEmpresaResponse {

    private Long id;

    private AcaoResponse acao;

    @Schema(description = "Corresponde à quantidade comprada desta ação")
    private Integer quantidade;

    @Schema(description = "Preço total, considerando o preço da ação com a quantidade comprada (<code>acao.preco * quantidade</code>).")
    public Float getValorTotal() {
        return roundCurrency(acao.getPreco() * quantidade);
    }
}
