package net.gustavodias.desafioInter.config;

import lombok.experimental.UtilityClass;

@UtilityClass
public class Constants {

    @UtilityClass
    public static class CacheNames {
        public static final String ACAO = "acao";
        public static final String ACOES = "acoes";
        public static final String ACOES_MELHORES = "acoesMelhores";
    }
}
