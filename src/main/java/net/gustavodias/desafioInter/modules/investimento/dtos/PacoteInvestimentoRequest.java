package net.gustavodias.desafioInter.modules.investimento.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
public class PacoteInvestimentoRequest {

    @NotNull
    @Positive
    @Schema(description = "Quantidade de ações distintas que o investidor quer diversificar no seu investimento")
    private Integer quantidadeDeAcoes;

    @NotNull
    @Positive
    @Schema(description = "Valor em dinheiro que ele quer investir." +
        " Será o máximo valor que ele irá aplicar, contando que pode ter troco." +
        " Pois não é possível comprar frações de quantidades de ações.")
    private Float valorAInvestir;
}
