package net.gustavodias.desafioInter.modules.investimento.dtos;

import net.gustavodias.desafioInter.modules.acao.dtos.AcaoResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class InvestimentoResponseTest {

    @Test
    @DisplayName("getValorTotalAcoes." +
        " Soma dos valores das ações, com quantidades." +
        " Quando houver mais de uma ação.")
    public void getValorTotalAcoes_somaDosValoresDasAcoesComQuantidades_quandoHouverMaisDeUmaAcao() {
        var investimento = umInvestimentoComValoresTotaisAcoes(54.17f, 102.00f, 8.99f);
        assertThat(investimento.getValorTotalAcoes()).isEqualTo(165.16f);
    }

    @Test
    @DisplayName("getValorTotalAcoes." +
        " Valor total arredondado com precisão de moeda." +
        " Quando os valores tiverem perda de precisão no numero.")
    public void getValorTotalAcoes_valorTotalArredondadoComPrecisaoDeMoeda_quandoOsValoresPerderemPrecisaoNumero() {
        var investimento = umInvestimentoComValoresTotaisAcoes(241.62999f, 17.83999f, 100.4001f);
        assertThat(investimento.getValorTotalAcoes()).isEqualTo(359.87f);
    }

    @Test
    @DisplayName("getValorTotalAcoes." +
        " Valor total da única ação." +
        " Quando houver apenas uma ação.")
    public void getValorTotalAcoes_valorTotalDaUnicaAcao_quandoHouverApenasUmaAcao() {
        var investimento = umInvestimentoComValoresTotaisAcoes(130.56f);
        assertThat(investimento.getValorTotalAcoes()).isEqualTo(130.56f);
    }

    @Test
    @DisplayName("getValorTotalAcoes." +
        " Zero." +
        " Quando não houver nenhuma ação.")
    public void getValorTotalAcoes_zero_quandoNaoHouverNenhumaAcao() {
        var investimento = umInvestimentoComValoresTotaisAcoes();
        assertThat(investimento.getValorTotalAcoes()).isZero();
    }

    @Test
    @DisplayName("getQuantidadeDeAcoesDistintas." +
        " Quantidade correta" +
        " Quando houverem várias ações.")
    public void getQuantidadeDeAcoesDistintas_quantidadeCorreta_quandoHouveremVariasAcoes() {
        var investimento = InvestimentoResponse.builder()
            .investimentoEmpresas(IntStream.range(0, 7).mapToObj(i -> new InvestimentoEmpresaResponse()).toList())
            .build();
        assertThat(investimento.getQuantidadeDeAcoesDistintas()).isEqualTo(7);
    }

    @Test
    @DisplayName("getTroco." +
        " Valor correto." +
        " Quando o valor total das ações for válido")
    public void getTroco_valorCorreto_quandoOValorTotalAcoesForValido() {
        var investimento = spy(InvestimentoResponse.builder()
            .valorInvestido(102.47f)
            .investimentoEmpresas(List.of())
            .build());
        when(investimento.getValorTotalAcoes()).thenReturn(98.71f);

        assertThat(investimento.getTroco()).isEqualTo(3.76f);
    }

    @Test
    @DisplayName("getTroco." +
        " Valor arredondado com precisão de moeda." +
        " Quando o valor investido tiver perda de precisão no número.")
    public void getTroco_valorArredondadoComPrecisaoDeMoeda_quandoValorInvestidoPerderPrecisaoNumero() {
        var investimento = spy(InvestimentoResponse.builder()
            .valorInvestido(150.44999f)
            .investimentoEmpresas(List.of())
            .build());
        when(investimento.getValorTotalAcoes()).thenReturn(12.10f);

        assertThat(investimento.getTroco()).isEqualTo(138.35f);
    }

    private InvestimentoResponse umInvestimentoComValoresTotaisAcoes(Float... valoresTotais) {
        return InvestimentoResponse.builder()
            .investimentoEmpresas(umInvestimentoEmpresaResponseComValorTotalList(valoresTotais))
            .build();
    }

    private List<InvestimentoEmpresaResponse> umInvestimentoEmpresaResponseComValorTotalList(Float... valoresTotais) {
        return Stream.of(valoresTotais).map(this::umInvestimentoEmpresaResponseComValorTotal).toList();
    }

    private InvestimentoEmpresaResponse umInvestimentoEmpresaResponseComValorTotal(Float valorTotal) {
        var investimentoEmpresa = spy(InvestimentoEmpresaResponse.builder()
            .acao(AcaoResponse.builder().preco(0f).build())
            .quantidade(0)
            .build());
        when(investimentoEmpresa.getValorTotal()).thenReturn(valorTotal);
        return investimentoEmpresa;
    }
}
