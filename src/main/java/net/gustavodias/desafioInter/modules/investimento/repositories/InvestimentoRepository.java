package net.gustavodias.desafioInter.modules.investimento.repositories;

import net.gustavodias.desafioInter.modules.commons.repositories.BaseRepository;
import net.gustavodias.desafioInter.modules.investimento.models.Investimento;

public interface InvestimentoRepository extends BaseRepository<Investimento, Long> {
}
