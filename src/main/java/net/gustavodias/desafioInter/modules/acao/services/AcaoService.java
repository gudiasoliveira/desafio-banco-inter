package net.gustavodias.desafioInter.modules.acao.services;

import net.gustavodias.desafioInter.config.Constants;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPatchRequest;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPostRequest;
import net.gustavodias.desafioInter.modules.acao.filters.AcaoFilters;
import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.acao.repositories.AcaoRepository;
import net.gustavodias.desafioInter.modules.commons.exceptions.NotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.cache.annotation.Caching;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AcaoService {

    @Autowired
    private AcaoRepository repository;

    @Autowired
    private AcaoValidationsService validationsService;

    @Cacheable(value = Constants.CacheNames.ACAO, key = "#ticker")
    public Acao getByTicker(String ticker) {
        return repository.findById(ticker)
            .orElseThrow(() -> new NotFoundException(String.format("Nenhuma ação encontrada com o ticker \"%s\"", ticker)));
    }

    @Cacheable(Constants.CacheNames.ACOES)
    public Iterable<Acao> getAll(AcaoFilters filters) {
        return repository.findAll(filters.toPredicate());
    }

    @Cacheable(Constants.CacheNames.ACOES_MELHORES)
    public List<Acao> findMelhores(int quantidadeDeAcoes, float valorAInvestir) {
        return repository.findMelhores(quantidadeDeAcoes, valorAInvestir);
    }

    @Caching(
        put = @CachePut(value = Constants.CacheNames.ACAO, key = "#request.ticker"),
        evict = {
            @CacheEvict(value = Constants.CacheNames.ACOES, allEntries = true),
            @CacheEvict(value = Constants.CacheNames.ACOES_MELHORES, allEntries = true)
        }
    )
    public Acao create(AcaoPostRequest request) {
        validationsService.validateNotExists(request.getTicker());
        var acao = Acao.of(request);
        return repository.save(acao);
    }

    @Caching(
        put = @CachePut(value = Constants.CacheNames.ACAO, key = "#request.ticker"),
        evict = {
            @CacheEvict(value = Constants.CacheNames.ACOES, allEntries = true),
            @CacheEvict(value = Constants.CacheNames.ACOES_MELHORES, allEntries = true)
        }
    )
    public Acao update(AcaoPatchRequest request) {
        var acao = getByTicker(request.getTicker());
        acao.edit(request);
        return repository.save(acao);
    }

    @Caching(
        evict = {
            @CacheEvict(value = Constants.CacheNames.ACAO, key = "#ticker"),
            @CacheEvict(value = Constants.CacheNames.ACOES, allEntries = true),
            @CacheEvict(value = Constants.CacheNames.ACOES_MELHORES, allEntries = true)
        }
    )
    public void delete(String ticker) {
        validationsService.validateExists(ticker);
        validationsService.validateHasNoInvestimentos(ticker);
        repository.deleteById(ticker);
    }
}
