package net.gustavodias.desafioInter.modules.acao.repositories;

import net.gustavodias.desafioInter.modules.acao.models.Acao;

import java.util.List;

public interface AcaoCustomRepository {
    List<Acao> findMelhores(int quantidadeDeAcoes, float valorAInvestir);
}
