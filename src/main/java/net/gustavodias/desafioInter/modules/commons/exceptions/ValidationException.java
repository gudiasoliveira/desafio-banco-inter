package net.gustavodias.desafioInter.modules.commons.exceptions;

import org.springframework.http.HttpStatus;

public class ValidationException extends ApiErrorException {

    public ValidationException(String message) {
        super(message, HttpStatus.BAD_REQUEST);
    }
}
