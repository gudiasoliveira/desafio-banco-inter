package net.gustavodias.desafioInter.modules.investimento.repositories;

import net.gustavodias.desafioInter.modules.commons.repositories.BaseRepository;
import net.gustavodias.desafioInter.modules.investimento.models.InvestimentoEmpresa;

public interface InvestimentoEmpresaRepository extends BaseRepository<InvestimentoEmpresa, Long> {

    boolean existsByAcaoTicker(String acaoTicker);
}
