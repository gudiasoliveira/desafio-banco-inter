create domain active_status as enum('A', 'I');

create domain currency as numeric(19, 2);
create domain id_type as bigint;
create domain acao_ticker_type as character varying (10);

create table acao (
    ticker acao_ticker_type not null,
    nome character varying (120) not null,
    preco currency not null,
    ativa active_status default 'A' not null,
    constraint pk_acao primary key (ticker)
);

create table investimento_empresa (
    id id_type not null auto_increment,
    acao_ticker acao_ticker_type not null,
    quantidade int not null,
    investimento_id id_type not null,
    constraint pk_investimento_empresa primary key(id),
    constraint fk_invemp_acao foreign key (acao_ticker) references acao(ticker),
    constraint chk_invemp_quantidade_positiva check (quantidade > 0)
);

create table investimento (
    id id_type not null auto_increment,
    valor_investido currency not null,
    constraint pk_investimento primary key (id)
);

alter table investimento_empresa add constraint fk_invemp_investimento foreign key (investimento_id) references investimento(id);
