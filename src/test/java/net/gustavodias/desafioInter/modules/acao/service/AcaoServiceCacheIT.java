package net.gustavodias.desafioInter.modules.acao.service;

import net.gustavodias.desafioInter.config.Constants;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPatchRequest;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPostRequest;
import net.gustavodias.desafioInter.modules.acao.filters.AcaoFilters;
import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.acao.services.AcaoService;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import net.gustavodias.desafioInter.modules.commons.exceptions.NotFoundException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Objects;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.assertj.core.api.Assertions.*;
import static org.assertj.core.data.Offset.offset;
import static org.assertj.core.groups.Tuple.tuple;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Sql("classpath:test-sql-scripts/acao/acao-service-it.sql")
public class AcaoServiceCacheIT {

    @Autowired
    private AcaoService service;
    @Autowired
    private CacheManager cacheManager;

    @BeforeEach
    public void beforeEach() {
        Stream.of(Constants.CacheNames.ACAO, Constants.CacheNames.ACOES, Constants.CacheNames.ACOES_MELHORES)
            .map(cacheManager::getCache)
            .filter(Objects::nonNull)
            .forEach(Cache::clear);
    }

    @Test
    @DisplayName("getByTicker." +
        " Deve retornar a ação recém-criada, a partir do cache atualizado." +
        " Quando depois de criar uma nova.")
    public void getByTicker_deveRetornarAAcaoRecemCriadaAPartirDoCacheAtualizado_quandoDepoisDeCriarUmaNova() {
        var ticker = "PBR45";

        assertThatExceptionOfType(NotFoundException.class)
            .isThrownBy(() -> service.getByTicker(ticker))
            .withMessage(String.format("Nenhuma ação encontrada com o ticker \"%s\"", ticker));

        var request = AcaoPostRequest.builder()
            .ticker(ticker)
            .nome("Petrobras")
            .preco(78.09f)
            .ativo(EActiveStatus.A)
            .build();
        service.create(request);

        assertThatCode(() -> service.getByTicker(ticker)).doesNotThrowAnyException();
    }

    @Test
    @DisplayName("getByTicker." +
        " Deve retornar a ação com os dados atualizados." +
        " Quando depois de atualizar.")
    public void getByTicker_deveRetornarAAcaoComOsDadosAtualizados_quandoDepoisDeAtualizar() {
        var ticker = "EGIE3";
        var preco = 39.00f;

        assertThat(service.getByTicker(ticker).getPreco()).isNotCloseTo(preco, offset(0.001f));

        var request = AcaoPatchRequest.builder()
            .ticker(ticker)
            .ativo(EActiveStatus.I)
            .preco(preco)
            .build();
        service.update(request);

        assertThat(service.getByTicker(ticker).getPreco()).isCloseTo(preco, offset(0.001f));
    }

    @Test
    @DisplayName("getByTicker." +
        " Deve retornar erro de ação não encontrada, com o cache atualizado." +
        " Quando depois de excluir a ação.")
    public void getByTicker_deveRetornarErroDeAcaoNaoEncontradaComOCacheAtualizado_quandoDepoisDeExcluirAAcao() {
        var ticker = "LREN3";

        assertThatCode(() -> service.getByTicker(ticker)).doesNotThrowAnyException();

        service.delete(ticker);

        assertThatExceptionOfType(NotFoundException.class)
            .isThrownBy(() -> service.getByTicker(ticker))
            .withMessage(String.format("Nenhuma ação encontrada com o ticker \"%s\"", ticker));
    }

    @Test
    @DisplayName("getAll." +
        " Deve retornar a lista com a ação recém-criada, a partir do cache atualizado." +
        " Quando depois de criar uma nova.")
    public void getAll_deveRetornarAListaComAAcaoRecemCriadaAPartirDoCacheAtualizado_quandoDepoisDeCriarUmaNova() {
        var ticker = "PBR45";

        var prevAcoesCount = (int) StreamSupport.stream(service.getAll(new AcaoFilters()).spliterator(), false).count();

        var request = AcaoPostRequest.builder()
            .ticker(ticker)
            .nome("Petrobras")
            .preco(78.09f)
            .ativo(EActiveStatus.A)
            .build();
        service.create(request);

        assertThat(service.getAll(new AcaoFilters())).hasSize(prevAcoesCount + 1);
    }

    @Test
    @DisplayName("getAll." +
        " Deve retornar lista de ações com os dados da ação atualizados." +
        " Quando depois de atualizar.")
    public void getAll_deveRetornarAListaDeAcoesComOsDadosDaAcaoAtualizados_quandoDepoisDeAtualizar() {
        var ticker = "EGIE3";
        var preco = 39.00f;

        assertThat(service.getAll(new AcaoFilters()))
            .filteredOn(acao -> Objects.equals(acao.getTicker(), ticker))
            .first()
            .satisfies(acao -> assertThat(acao.getPreco()).isNotCloseTo(preco, offset(0.001f)));

        var request = AcaoPatchRequest.builder()
            .ticker(ticker)
            .ativo(EActiveStatus.I)
            .preco(preco)
            .build();
        service.update(request);

        assertThat(service.getAll(new AcaoFilters()))
            .filteredOn(acao -> Objects.equals(acao.getTicker(), ticker))
            .first()
            .satisfies(acao -> assertThat(acao.getPreco()).isCloseTo(preco, offset(0.001f)));
    }

    @Test
    @DisplayName("getAll." +
        " Deve retornar a lista de ações sem a ação recém excluída, com o cache atualizado." +
        " Quando depois de excluir a ação.")
    public void getAll_deveRetornarErroDeAcaoNaoEncontradaComOCacheAtualizado_quandoDepoisDeExcluirAAcao() {
        var ticker = "LREN3";

        var prevAcoesCount = (int) StreamSupport.stream(service.getAll(new AcaoFilters()).spliterator(), false).count();

        service.delete(ticker);

        assertThat(service.getAll(new AcaoFilters())).hasSize(prevAcoesCount - 1);
    }

    @Test
    @DisplayName("findMelhores." +
        " Deve retornar a lista de ações atualizada." +
        " Quando depois de criar uma nova.")
    public void findMelhores_deveRetornarAListaDeAcoesAtualizada_quandoDepoisDeCriarUmaNova() {
        var quantidadeDeAcoes = 3;
        var valorAInvestir = 40.00f;

        assertThat(service.findMelhores(quantidadeDeAcoes, valorAInvestir))
            .extracting(Acao::getTicker)
            .containsExactly("AMAR3", "MGLU4", "SULA11");

        var request = AcaoPostRequest.builder()
            .ticker("PBR45")
            .nome("Petrobrás")
            .preco(5.10f)
            .ativo(EActiveStatus.A)
            .build();
        service.create(request);

        assertThat(service.findMelhores(quantidadeDeAcoes, valorAInvestir))
            .extracting(Acao::getTicker)
            .containsExactly("PBR45", "AMAR3", "MGLU4");
    }

    @Test
    @DisplayName("findMelhores." +
        " Deve retornar a lista de ações atualizada." +
        " Quando depois de atualizar uma.")
    public void findMelhores_deveRetornarAListaDeAcoesAtualizada_quandoDepoisDeAtualizarUma() {
        var quantidadeDeAcoes = 3;
        var valorAInvestir = 40.00f;

        assertThat(service.findMelhores(quantidadeDeAcoes, valorAInvestir))
            .extracting("ticker", "preco")
            .containsExactly(
                tuple("AMAR3", 6.30f),
                tuple("MGLU4", 18.80f),
                tuple("SULA11", 28.26f)
            );

        var request = AcaoPatchRequest.builder()
            .ticker("MGLU4")
            .preco(31.02f)
            .build();
        service.update(request);

        assertThat(service.findMelhores(quantidadeDeAcoes, valorAInvestir))
            .extracting("ticker", "preco")
            .containsExactly(
                tuple("AMAR3", 6.30f),
                tuple("SULA11", 28.26f),
                tuple("MGLU4", 31.02f)
            );
    }

    @Test
    @DisplayName("findMelhores." +
        " Deve retornar a lista de ações atualizada." +
        " Quando depois de excluir uma.")
    public void findMelhores_deveRetornarAListaDeAcoesAtualizada_quandoDepoisDeExcluirUma() {
        var quantidadeDeAcoes = 3;
        var valorAInvestir = 40.00f;

        assertThat(service.findMelhores(quantidadeDeAcoes, valorAInvestir))
            .extracting(Acao::getTicker)
            .containsExactly("AMAR3", "MGLU4", "SULA11");

        service.delete("MGLU4");

        assertThat(service.findMelhores(quantidadeDeAcoes, valorAInvestir))
            .extracting(Acao::getTicker)
            .containsExactly("AMAR3", "SULA11", "LREN3");
    }
}
