package net.gustavodias.desafioInter.modules.commons.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum EActiveStatus {
    A("Ativo"),
    I("Inativo");

    private final String descricao;
}
