package net.gustavodias.desafioInter.modules.acao.services;

import net.gustavodias.desafioInter.modules.acao.repositories.AcaoRepository;
import net.gustavodias.desafioInter.modules.commons.exceptions.NotFoundException;
import net.gustavodias.desafioInter.modules.commons.exceptions.ValidationException;
import net.gustavodias.desafioInter.modules.investimento.repositories.InvestimentoEmpresaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class AcaoValidationsService {

    @Autowired
    private AcaoRepository repository;
    @Autowired
    private InvestimentoEmpresaRepository investimentoEmpresaRepository;

    public void validateExists(String ticker) {
        if (!repository.existsById(ticker))
            throw new NotFoundException(String.format("Nenhuma ação encontrada com o ticker \"%s\"", ticker));
    }

    public void validateNotExists(String ticker) {
        if (repository.existsById(ticker))
            throw new ValidationException(String.format("Já existe uma ação com o ticker \"%s\"", ticker));
    }

    public void validateHasNoInvestimentos(String ticker) {
        if (investimentoEmpresaRepository.existsByAcaoTicker(ticker))
            throw new ValidationException(String.format("A ação \"%s\" possui investimentos cadastrados.", ticker));
    }
}
