## Pré-requisitos

- Ter o [Maven](https://maven.apache.org/) instalado na máquina
- Ter o [Java **17**](https://www.oracle.com/java/technologies/javase/jdk17-archive-downloads.html) instalado na máquina. O projeto é executado nesta versão.

Confira nas seções abaixo, com algumas instruções sobre uma das formas de instalar estas ferramentas, com o SDKMAN.

## Primeiros passos

Instale as dependências, e compile o projeto.

```bash
mvn clean install -Dmaven.test.skip
```

## Para executar e usar a aplicação

Execute o seguinte comando

```bash
mvn spring-boot:run
```

A api estará rodando na url `http://localhost:8080`.

Acesse `http://localhost:8080/swagger-ui/index.html` no seu navegador para usar a aplicação e acessar a documentação da API.

## Para executar os testes unitários e de integração

```bash
mvn test
```

## Instalando o Java e o Maven, com o SDKMAN

O [SDKMAN](https://sdkman.io/) é um software de gerenciamento de instalações de diversos SDKs, incluindo o Java e o Maven, permitindo instalar várias versões do mesmo SDKs na mesma máquina, podendo ir alternando entre elas.

1. [Acesse aqui](https://sdkman.io/install) as instruções de instalação
2. Instale o Maven:
```bash
sdk install maven 3.8.6 
```
3. Instale o Java JDK
```bash
sdk install java 17.0.4-zulu
```
4. Prontinho! Só retornar nas seções anteriores deste Readme, para seguir os passos de compilação e execução da aplicação e dos testes.
