package net.gustavodias.desafioInter.modules.acao.predicates;

import net.gustavodias.desafioInter.modules.acao.models.QAcao;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import net.gustavodias.desafioInter.modules.commons.predicates.BasePredicate;
import org.springframework.util.ObjectUtils;

import java.util.Objects;

public class AcaoPredicate extends BasePredicate {

    private final QAcao acao;

    public AcaoPredicate() {
        this(QAcao.acao);
    }

    public AcaoPredicate(QAcao acao) {
        this.acao = acao;
    }

    public AcaoPredicate withAtivo(EActiveStatus ativo) {
        if (Objects.nonNull(ativo)) {
            predicate.and(acao.ativa.eq(ativo));
        }
        return this;
    }

    public AcaoPredicate withNomeContainsIgnoreCase(String nome) {
        if (!ObjectUtils.isEmpty(nome)) {
            predicate.and(acao.nome.containsIgnoreCase(nome));
        }
        return this;
    }
}
