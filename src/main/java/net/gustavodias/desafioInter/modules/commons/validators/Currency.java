package net.gustavodias.desafioInter.modules.commons.validators;

import javax.validation.constraints.Digits;
import java.lang.annotation.*;

@Digits(integer = 19, fraction = 2)
@Documented
@Target({
    ElementType.METHOD,
    ElementType.FIELD,
    ElementType.ANNOTATION_TYPE,
    ElementType.CONSTRUCTOR,
    ElementType.PARAMETER
})
@Retention(RetentionPolicy.RUNTIME)
public @interface Currency {
}
