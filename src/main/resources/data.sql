insert into acao(ticker, nome, preco, ativa) values
    ('BIDI11', 'Inter', 66.51, 'A'),
    ('MGLU3', 'Magazine Luíza', 18.80, 'A'),
    ('SULA11', 'Sulamérica', 28.26, 'A'),
    ('EGIE3', 'Engie', 38.30, 'I'),
    ('CVCB3', 'CVC', 20.87, 'I'),
    ('LREN3', 'Renner', 36.95, 'A'),
    ('AMAR3', 'Marisa', 6.30, 'A');

insert into investimento(valor_investido) values
    (85.22), -- ID 1 -- troco 9.90
    (105.80); -- ID 2 -- troco 12.35

insert into investimento_empresa(acao_ticker, quantidade, investimento_id) values
    ('SULA11', 2, 1), -- ID 1
    ('MGLU3', 1, 1), -- ID 2
    ('LREN3', 1, 2), -- ID 3
    ('AMAR3', 3, 2), -- ID 4
    ('MGLU3', 2, 2); -- ID 5
