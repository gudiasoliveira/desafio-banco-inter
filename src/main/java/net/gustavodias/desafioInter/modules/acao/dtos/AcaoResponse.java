package net.gustavodias.desafioInter.modules.acao.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.gustavodias.desafioInter.modules.acao.mappers.AcaoMapper;
import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;

import java.util.List;
import java.util.Optional;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcaoResponse {

    private String ticker;

    @Schema(description = "Corresponde ao nome da empresa que vende esta ação")
    private String nome;

    @Schema(description = "Preço unitário da ação")
    private Float preco;

    private EActiveStatus ativo;

    public String getAtivoDescricao() {
        return Optional.ofNullable(ativo).map(EActiveStatus::getDescricao).orElse(null);
    }

    public static AcaoResponse of(Acao acao) {
        return AcaoMapper.INSTANCE.modelToResponse(acao);
    }

    public static List<AcaoResponse> listOf(Iterable<Acao> acoes) {
        return AcaoMapper.INSTANCE.modelToResponseList(acoes);
    }
}
