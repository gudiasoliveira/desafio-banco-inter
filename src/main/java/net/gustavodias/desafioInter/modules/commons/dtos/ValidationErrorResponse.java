package net.gustavodias.desafioInter.modules.commons.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Getter;
import lombok.experimental.SuperBuilder;
import org.springframework.validation.FieldError;

import javax.validation.ConstraintViolation;

@Getter
@SuperBuilder
public class ValidationErrorResponse extends ApiErrorResponse {

    @Schema(description = "Nome ou caminho do campo inválido")
    private String field;
    @Schema(description = "O valor inválido")
    private Object value;

    public static ValidationErrorResponse of(FieldError error) {
        return builder()
            .errorMessage(error.getDefaultMessage())
            .field(error.getField())
            .value(error.getRejectedValue())
            .build();
    }

    public static ValidationErrorResponse of(ConstraintViolation<?> error) {
        return builder()
            .errorMessage(error.getMessage())
            .field(error.getPropertyPath().toString())
            .value(error.getInvalidValue())
            .build();
    }
}
