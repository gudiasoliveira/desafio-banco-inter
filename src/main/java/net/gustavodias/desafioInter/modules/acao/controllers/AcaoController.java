package net.gustavodias.desafioInter.modules.acao.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPatchRequest;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPostRequest;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoResponse;
import net.gustavodias.desafioInter.modules.acao.filters.AcaoFilters;
import net.gustavodias.desafioInter.modules.acao.services.AcaoService;
import net.gustavodias.desafioInter.modules.commons.dtos.ApiErrorResponse;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("acao")
public class AcaoController {

    @Autowired
    private AcaoService service;

    @Operation(
        summary = "Consultar lista de ações cadastradas",
        description = "É possível passar alguns filtros para a consulta"
    )
    @GetMapping
    public Iterable<AcaoResponse> getAll(AcaoFilters filters) {
        return AcaoResponse.listOf(service.getAll(filters));
    }

    @Operation(
        summary = "Cria uma ação",
        responses = {
            @ApiResponse(
                responseCode = "400",
                description = "Quando já existir uma outra ação com o mesmo ticker",
                content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))
            )
        }
    )
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public AcaoResponse create(@RequestBody @Valid AcaoPostRequest request) {
        return AcaoResponse.of(service.create(request));
    }

    @Operation(
        summary = "Atualiza alguns dados da ação",
        description = "A atualização é parcial, ou seja, atualizará apenas os campos que forem passados na requisição." +
            " O campo <code>ticker</code> especificará qual ação deve ser atualizada com os valores dos demais campos.",
        responses = {
            @ApiResponse(
                responseCode = "404",
                description = "Quando não existir nenhuma ação com o ticker passado",
                content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))
            )
        }
    )
    @PatchMapping
    @ResponseStatus(HttpStatus.OK)
    public AcaoResponse update(@RequestBody @Valid AcaoPatchRequest request) {
        return AcaoResponse.of(service.update(request));
    }

    @Operation(
        summary = "Exclui uma ação",
        responses = {
            @ApiResponse(
                responseCode = "404",
                description = "Quando não existir nenhuma ação com o ticker passado",
                content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))
            ),
            @ApiResponse(
                responseCode = "400",
                description = "O sistema não permitirá a exclusão de ações para as quais tem investimentos cadastrados",
                content = @Content(schema = @Schema(implementation = ApiErrorResponse.class))
            )
        }
    )
    @DeleteMapping("{ticker}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable String ticker) {
        service.delete(ticker);
    }
}
