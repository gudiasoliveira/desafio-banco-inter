package net.gustavodias.desafioInter.modules.acao.service;

import net.gustavodias.desafioInter.config.Constants;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPatchRequest;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPostRequest;
import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.acao.repositories.AcaoRepository;
import net.gustavodias.desafioInter.modules.acao.services.AcaoService;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import net.gustavodias.desafioInter.modules.commons.exceptions.NotFoundException;
import net.gustavodias.desafioInter.modules.commons.exceptions.ValidationException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.Objects;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatExceptionOfType;
import static org.assertj.core.data.Offset.offset;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Sql("classpath:test-sql-scripts/acao/acao-service-it.sql")
public class AcaoServiceIT {

    @Autowired
    private AcaoService service;
    @Autowired
    private AcaoRepository repository;
    @Autowired
    private CacheManager cacheManager;

    @BeforeEach
    public void beforeEach() {
        Stream.of(Constants.CacheNames.ACAO, Constants.CacheNames.ACOES)
            .map(cacheManager::getCache)
            .filter(Objects::nonNull)
            .forEach(Cache::clear);
    }

    @Test
    @DisplayName("getByTicker." +
        " Ação." +
        " Quando existir com o ticker.")
    public void getByTicker_acao_quandoExistirComOTicker() {
        var ticker = "SULA11";
        assertThat(repository.existsById(ticker)).isTrue();
        assertThat(service.getByTicker(ticker))
            .hasFieldOrPropertyWithValue("ticker", "SULA11")
            .hasFieldOrPropertyWithValue("nome", "Sulamérica")
            .hasFieldOrPropertyWithValue("preco", 28.26f)
            .hasFieldOrPropertyWithValue("ativa", EActiveStatus.A);
    }

    @Test
    @DisplayName("getByTicker." +
        " Not Found Exception." +
        " Quando não existir com o ticker.")
    public void getByTicker_notFoundException_quandoNaoExistirAcaoComOTicker() {
        var ticker = "ABC123";
        assertThat(repository.existsById(ticker)).isFalse();
        assertThatExceptionOfType(NotFoundException.class)
            .isThrownBy(() -> service.getByTicker(ticker))
            .withMessage("Nenhuma ação encontrada com o ticker \"ABC123\"");
    }

    @Test
    @DisplayName("create." +
        " Deve criar e retornar a nova ação." +
        " Quando ainda não existir outra com o mesmo ticker.")
    public void create_deveCriarERetornarANovaAcao_quandoNaoExistirOutraComOTicker() {
        var ticker = "PBR45";
        assertThat(repository.existsById(ticker)).isFalse();

        var request = AcaoPostRequest.builder()
            .ticker(ticker)
            .nome("Petrobras")
            .preco(78.09f)
            .ativo(EActiveStatus.A)
            .build();
        assertThat(service.create(request))
            .hasFieldOrPropertyWithValue("ticker", "PBR45")
            .hasFieldOrPropertyWithValue("nome", "Petrobras")
            .hasFieldOrPropertyWithValue("preco", 78.09f)
            .hasFieldOrPropertyWithValue("ativa", EActiveStatus.A);
        assertThat(repository.existsById(ticker)).isTrue();
    }

    @Test
    @DisplayName("create." +
        " Validation Exception." +
        " Quando já existir outra ação com o mesmo ticker.")
    public void create_validationException_quandoJaExistirOutraAcaoComOTicker() {
        var ticker = "EGIE3";
        assertThat(repository.existsById(ticker)).isTrue();

        var request = AcaoPostRequest.builder()
            .ticker(ticker)
            .nome("Engie 2")
            .preco(70.19f)
            .ativo(EActiveStatus.A)
            .build();
        assertThatExceptionOfType(ValidationException.class)
            .isThrownBy(() -> service.create(request))
            .withMessage("Já existe uma ação com o ticker \"EGIE3\"");
    }

    @Test
    @DisplayName("update." +
        " Deve editar a ação." +
        " Quando existir com o ticker.")
    public void update_deveEditarAAcao_quandoExistirComOTicker() {
        var ticker = "EGIE3";
        var preco = 39.00f;

        assertThat(repository.findById(ticker)).isPresent().get()
            .extracting(Acao::getPreco)
            .satisfies(p -> assertThat(p).isNotCloseTo(preco, offset(0.001f)));

        var request = AcaoPatchRequest.builder()
            .ticker(ticker)
            .ativo(EActiveStatus.I)
            .preco(preco)
            .build();
        service.update(request);

        assertThat(repository.findById(ticker)).isPresent().get()
            .hasFieldOrPropertyWithValue("ticker", "EGIE3")
            .hasFieldOrPropertyWithValue("nome", "Engie")
            .hasFieldOrPropertyWithValue("preco", preco)
            .hasFieldOrPropertyWithValue("ativa", EActiveStatus.I);
    }

    @Test
    @DisplayName("update." +
        " Not Found Exception." +
        " Quando não existir com o ticker.")
    public void update_notFoundException_quandoNaoExistirComOTicker() {
        var ticker = "PBR45";
        assertThat(repository.existsById(ticker)).isFalse();

        var request = AcaoPatchRequest.builder()
            .ticker(ticker)
            .ativo(EActiveStatus.I)
            .preco(32.10f)
            .build();
        assertThatExceptionOfType(NotFoundException.class)
            .isThrownBy(() -> service.update(request))
            .withMessage("Nenhuma ação encontrada com o ticker \"PBR45\"");
    }

    @Test
    @DisplayName("delete." +
        " Deve excluir a ação." +
        " Quando existir.")
    public void delete_deveExcluirAcao_quandoExistir() {
        var ticker = "LREN3";

        assertThat(repository.existsById(ticker)).isTrue();

        service.delete(ticker);

        assertThat(repository.existsById(ticker)).isFalse();
    }

    @Test
    @DisplayName("delete." +
        " Not Found Exception." +
        " Quando não existir.")
    public void delete_notFoundException_quandoNaoExistir() {
        var ticker = "PBR45";
        assertThat(repository.existsById(ticker)).isFalse();

        assertThatExceptionOfType(NotFoundException.class)
            .isThrownBy(() -> service.delete(ticker))
            .withMessage("Nenhuma ação encontrada com o ticker \"PBR45\"");
    }

    @Test
    @DisplayName("delete." +
        " Deve não permitir exclusão. " +
        " Quando a ação tiver investimentos. ")
    public void delete_deveNaoPermitirExclusao_quandoAcaoTiverInvestimentos() {
        assertThatExceptionOfType(ValidationException.class)
            .isThrownBy(() -> service.delete("BIDI13"))
            .withMessage("A ação \"BIDI13\" possui investimentos cadastrados.");
    }
}
