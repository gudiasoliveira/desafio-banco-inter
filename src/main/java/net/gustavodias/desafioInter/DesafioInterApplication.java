package net.gustavodias.desafioInter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
@EnableCaching
public class DesafioInterApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesafioInterApplication.class, args);
	}

}
