package net.gustavodias.desafioInter.modules.commons.exceptions;

import lombok.Getter;
import org.springframework.http.HttpStatus;

public class ApiErrorException extends RuntimeException {

    @Getter
    private final HttpStatus httpStatus;

    public ApiErrorException(String message, HttpStatus httpStatus) {
        super(message);
        this.httpStatus = httpStatus;
    }
}
