package net.gustavodias.desafioInter.modules.acao.filters;

import com.querydsl.core.BooleanBuilder;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.Data;
import net.gustavodias.desafioInter.modules.acao.predicates.AcaoPredicate;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import org.springdoc.api.annotations.ParameterObject;

@Data
@ParameterObject
public class AcaoFilters {

    @Parameter(description = "Filtro por nome, <strong>contendo</strong> a string, sem diferenciar maiúsculas de minúsculas")
    private String nome;
    @Parameter(description = "Filtra por estado <strong>ativo</strong> ou <strong>inativo</strong>")
    private EActiveStatus ativo;

    public BooleanBuilder toPredicate() {
        return new AcaoPredicate()
            .withNomeContainsIgnoreCase(nome)
            .withAtivo(ativo)
            .build();
    }
}
