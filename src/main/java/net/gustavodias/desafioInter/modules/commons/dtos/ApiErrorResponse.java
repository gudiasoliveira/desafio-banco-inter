package net.gustavodias.desafioInter.modules.commons.dtos;

import lombok.Getter;
import lombok.experimental.SuperBuilder;

@Getter
@SuperBuilder
public class ApiErrorResponse {
    private String errorMessage;

    public static ApiErrorResponse of(Exception exception) {
        return ApiErrorResponse.builder()
            .errorMessage(exception.getMessage())
            .build();
    }
}
