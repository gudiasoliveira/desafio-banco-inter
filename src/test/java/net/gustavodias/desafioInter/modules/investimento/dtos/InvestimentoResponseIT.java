package net.gustavodias.desafioInter.modules.investimento.dtos;

import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import net.gustavodias.desafioInter.modules.investimento.models.Investimento;
import net.gustavodias.desafioInter.modules.investimento.models.InvestimentoEmpresa;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class InvestimentoResponseIT {

    @Test
    @DisplayName("of." +
        " Response com dados corretos." +
        " Quando a model for válida.")
    public void of_responseComDadosCorretos_quandoAModelForValida() {
        var investimento = Investimento.builder()
            .id(12L)
            .valorInvestido(501.40f)
            .investimentoEmpresas(List.of(
                InvestimentoEmpresa.builder()
                    .id(25L)
                    .quantidade(3)
                    .acao(Acao.builder()
                        .ticker("BIDI14")
                        .nome("Banco Inter")
                        .preco(76.30f)
                        .ativa(EActiveStatus.A)
                        .build())
                    .build(),
                InvestimentoEmpresa.builder()
                    .id(4L)
                    .quantidade(2)
                    .acao(Acao.builder()
                        .ticker("LREN4")
                        .nome("Renner 4")
                        .preco(35.00f)
                        .ativa(EActiveStatus.I)
                        .build())
                    .build()
            ))
            .build();

        var response = InvestimentoResponse.of(investimento);

        assertThat(response)
            .hasFieldOrPropertyWithValue("id", 12L)
            .hasFieldOrPropertyWithValue("valorInvestido", 501.40f)
            .hasFieldOrPropertyWithValue("valorTotalAcoes", 298.90f)
            .hasFieldOrPropertyWithValue("quantidadeDeAcoesDistintas", 2)
            .hasFieldOrPropertyWithValue("troco", 202.50f);
        assertThat(response.getInvestimentoEmpresas()).hasSize(2);
        assertThat(response.getInvestimentoEmpresas().get(0))
            .hasFieldOrPropertyWithValue("id", 25L)
            .hasFieldOrPropertyWithValue("quantidade", 3)
            .hasFieldOrPropertyWithValue("valorTotal", 228.90f)
            .hasFieldOrPropertyWithValue("acao.ticker", "BIDI14")
            .hasFieldOrPropertyWithValue("acao.nome", "Banco Inter")
            .hasFieldOrPropertyWithValue("acao.preco", 76.30f)
            .hasFieldOrPropertyWithValue("acao.ativo", EActiveStatus.A)
            .hasFieldOrPropertyWithValue("acao.ativoDescricao", "Ativo");
        assertThat(response.getInvestimentoEmpresas().get(1))
            .hasFieldOrPropertyWithValue("id", 4L)
            .hasFieldOrPropertyWithValue("quantidade", 2)
            .hasFieldOrPropertyWithValue("valorTotal", 70.00f)
            .hasFieldOrPropertyWithValue("acao.ticker", "LREN4")
            .hasFieldOrPropertyWithValue("acao.nome", "Renner 4")
            .hasFieldOrPropertyWithValue("acao.preco", 35.00f)
            .hasFieldOrPropertyWithValue("acao.ativo", EActiveStatus.I)
            .hasFieldOrPropertyWithValue("acao.ativoDescricao", "Inativo");
    }

    @Test
    @DisplayName("listOf." +
        " Lista de responses com dados corretos." +
        " Quando as models forem válidas.")
    public void listOf_listaDeResponsesComDadosCorretos_quandoAsModelsForemValidas() {
        var investimentos = List.of(
            Investimento.builder()
                .id(4L)
                .valorInvestido(401.40f)
                .investimentoEmpresas(List.of(
                    InvestimentoEmpresa.builder()
                        .id(25L)
                        .quantidade(3)
                        .acao(Acao.builder()
                            .ticker("BIDI14")
                            .nome("Banco Inter")
                            .preco(76.30f)
                            .ativa(EActiveStatus.A)
                            .build())
                        .build(),
                    InvestimentoEmpresa.builder()
                        .id(1L)
                        .quantidade(1)
                        .acao(Acao.builder()
                            .ticker("CVCB3")
                            .nome("CVC")
                            .preco(20.00f)
                            .ativa(EActiveStatus.A)
                            .build())
                        .build()
                ))
                .build(),

            Investimento.builder()
                .id(76L)
                .valorInvestido(502.10f)
                .investimentoEmpresas(List.of(
                    InvestimentoEmpresa.builder()
                        .id(4L)
                        .quantidade(10)
                        .acao(Acao.builder()
                            .ticker("LREN4")
                            .nome("Renner 4")
                            .preco(35.00f)
                            .ativa(EActiveStatus.I)
                            .build())
                        .build()
                ))
                .build()
        );

        var responses = InvestimentoResponse.listOf(investimentos);

        assertThat(responses).hasSize(2);

        assertThat(responses.get(0))
            .hasFieldOrPropertyWithValue("id", 4L)
            .hasFieldOrPropertyWithValue("valorInvestido", 401.40f)
            .hasFieldOrPropertyWithValue("valorTotalAcoes", 248.90f)
            .hasFieldOrPropertyWithValue("quantidadeDeAcoesDistintas", 2)
            .hasFieldOrPropertyWithValue("troco", 152.50f);
        assertThat(responses.get(0).getInvestimentoEmpresas()).hasSize(2);
        assertThat(responses.get(0).getInvestimentoEmpresas().get(0))
            .hasFieldOrPropertyWithValue("id", 25L)
            .hasFieldOrPropertyWithValue("quantidade", 3)
            .hasFieldOrPropertyWithValue("valorTotal", 228.90f)
            .hasFieldOrPropertyWithValue("acao.ticker", "BIDI14")
            .hasFieldOrPropertyWithValue("acao.nome", "Banco Inter")
            .hasFieldOrPropertyWithValue("acao.preco", 76.30f)
            .hasFieldOrPropertyWithValue("acao.ativo", EActiveStatus.A)
            .hasFieldOrPropertyWithValue("acao.ativoDescricao", "Ativo");
        assertThat(responses.get(0).getInvestimentoEmpresas().get(1))
            .hasFieldOrPropertyWithValue("id", 1L)
            .hasFieldOrPropertyWithValue("quantidade", 1)
            .hasFieldOrPropertyWithValue("valorTotal", 20.00f)
            .hasFieldOrPropertyWithValue("acao.ticker", "CVCB3")
            .hasFieldOrPropertyWithValue("acao.nome", "CVC")
            .hasFieldOrPropertyWithValue("acao.preco", 20.00f)
            .hasFieldOrPropertyWithValue("acao.ativo", EActiveStatus.A)
            .hasFieldOrPropertyWithValue("acao.ativoDescricao", "Ativo");

        assertThat(responses.get(1))
            .hasFieldOrPropertyWithValue("id", 76L)
            .hasFieldOrPropertyWithValue("valorInvestido", 502.10f)
            .hasFieldOrPropertyWithValue("valorTotalAcoes", 350.00f)
            .hasFieldOrPropertyWithValue("quantidadeDeAcoesDistintas", 1)
            .hasFieldOrPropertyWithValue("troco", 152.10f);
        assertThat(responses.get(1).getInvestimentoEmpresas()).hasSize(1);
        assertThat(responses.get(1).getInvestimentoEmpresas().get(0))
            .hasFieldOrPropertyWithValue("id", 4L)
            .hasFieldOrPropertyWithValue("quantidade", 10)
            .hasFieldOrPropertyWithValue("valorTotal", 350.00f)
            .hasFieldOrPropertyWithValue("acao.ticker", "LREN4")
            .hasFieldOrPropertyWithValue("acao.nome", "Renner 4")
            .hasFieldOrPropertyWithValue("acao.preco", 35.00f)
            .hasFieldOrPropertyWithValue("acao.ativo", EActiveStatus.I)
            .hasFieldOrPropertyWithValue("acao.ativoDescricao", "Inativo");
    }
}
