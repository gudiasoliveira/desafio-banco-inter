package net.gustavodias.desafioInter.modules.investimento.models;

import net.gustavodias.desafioInter.modules.acao.models.Acao;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Offset.offset;

public class InvestimentoIT {

    private static final Investimento UM_INVESTIMENTO = umInvestimento();

    @Test
    @DisplayName("getValorTotal." +
        " Valor total das ações, com as quantidades." +
        " Quando houverem diferentes ações com diferentes quantidades.")
    public void getValorTotal_valorTotalDasAcoesComQuantidades_quandoVariasAcoesComDiferentesQuantidades() {
        assertThat(UM_INVESTIMENTO.getValorTotal()).isCloseTo(253.90f, offset(0.001f));
    }

    @Test
    @DisplayName("getTroco." +
        " Valor correto." +
        " Quando houverem diferentes ações com diferentes quantidades.")
    public void getValorTotal_valorCorreto_quandoVariasAcoesComDiferentesQuantidades() {
        assertThat(UM_INVESTIMENTO.getTroco()).isCloseTo(1.25f, offset(0.001f));
    }

    private static Investimento umInvestimento() {
        return Investimento.builder()
            .valorInvestido(255.15f)
            .investimentoEmpresas(List.of(
                umInvestimentoEmpresaComPrecoEQuantidade(12.30f, 3),
                umInvestimentoEmpresaComPrecoEQuantidade(50f, 4),
                umInvestimentoEmpresaComPrecoEQuantidade(8.50f, 2)
            ))
            .build();
    }

    private static InvestimentoEmpresa umInvestimentoEmpresaComPrecoEQuantidade(Float preco, Integer quantidade) {
        return InvestimentoEmpresa.builder()
            .acao(Acao.builder().preco(preco).build())
            .quantidade(quantidade)
            .build();
    }
}
