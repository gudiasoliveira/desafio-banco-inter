package net.gustavodias.desafioInter.modules.acao.repositories;

import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.commons.repositories.BaseRepository;

public interface AcaoRepository extends BaseRepository<Acao, String>, AcaoCustomRepository {
}
