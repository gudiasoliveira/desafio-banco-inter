package net.gustavodias.desafioInter.modules.commons.utils;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static net.gustavodias.desafioInter.modules.commons.utils.NumberUtils.roundCurrency;
import static org.assertj.core.api.Assertions.assertThat;

public class NumberUtilsTest {

    @Test
    @DisplayName("roundCurrency." +
        " Deve arredondar para cima." +
        " Quando o algarismo duvidoso for maior que 5.")
    public void roundCurrency_deveArredondarParaCima_quandoAlgarismoDuvidosoMaiorQue5() {
        assertThat(roundCurrency(12.516f)).isEqualTo(12.52f);
        assertThat(roundCurrency(53.67999998f)).isEqualTo(53.68f);
        assertThat(roundCurrency(102.8999f)).isEqualTo(102.90f);
    }

    @Test
    @DisplayName("roundCurrency." +
        " Deve arredondar para baixo." +
        " Quando o algarismo duvidoso for menor que 5.")
    public void roundCurrency_deveArredondarParaBaixo_quandoAlgarismoDuvidosoMenorQue5() {
        assertThat(roundCurrency(20.5347f)).isEqualTo(20.53f);
        assertThat(roundCurrency(1200.100001f)).isEqualTo(1200.10f);
        assertThat(roundCurrency(10.790001f)).isEqualTo(10.79f);
    }

    @Test
    @DisplayName("roundCurrency." +
        " Deve arredondar para cima." +
        " Quando o algarismo duvidoso for 5.")
    public void roundCurrency_deveArredondarParaCima_quandoAlgarismoDuvidosoFor5() {
        assertThat(roundCurrency(19.315f)).isEqualTo(19.32f);
        assertThat(roundCurrency(72.48501f)).isEqualTo(72.49f);
        assertThat(roundCurrency(31.6759f)).isEqualTo(31.68f);
    }
}
