package net.gustavodias.desafioInter.modules.investimento.dtos;

import net.gustavodias.desafioInter.modules.acao.dtos.AcaoResponse;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class InvestimentoEmpresaResponseTest {

    @Test
    @DisplayName("getValorTotal." +
        " Deve arredondar para precisão de moeda." +
        " Quando o preço da ação tiver perda de precisão no número.")
    public void getValorTotal_deveArredondarParaPrecisaoDeMoeda_quandoAcaoTiverPerdaDePrecisaoNoNumero() {
        var investimentoEmpresa = InvestimentoEmpresaResponse.builder()
            .acao(AcaoResponse.builder().preco(29.39999f).build())
            .quantidade(3)
            .build();
        assertThat(investimentoEmpresa.getValorTotal()).isEqualTo(88.20f);
    }
}
