package net.gustavodias.desafioInter.modules.acao.mappers;

import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPostRequest;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoResponse;
import net.gustavodias.desafioInter.modules.acao.models.Acao;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper
public interface AcaoMapper {

    AcaoMapper INSTANCE = Mappers.getMapper(AcaoMapper.class);

    @Mapping(target = "ativa", source = "ativo")
    Acao postRequestToModel(AcaoPostRequest request);

    @Mapping(target = "ativo", source = "ativa")
    AcaoResponse modelToResponse(Acao acao);

    List<AcaoResponse> modelToResponseList(Iterable<Acao> acoes);
}
