package net.gustavodias.desafioInter.modules.commons.controllers;

import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import net.gustavodias.desafioInter.modules.commons.dtos.ApiErrorResponse;
import net.gustavodias.desafioInter.modules.commons.dtos.ValidationErrorResponse;
import net.gustavodias.desafioInter.modules.commons.exceptions.ApiErrorException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import javax.validation.ConstraintViolationException;

@RestControllerAdvice
public class ExceptionHandlersController extends ResponseEntityExceptionHandler {

    @ExceptionHandler(ApiErrorException.class)
    protected ResponseEntity<ApiErrorResponse> handleApiErrorException(ApiErrorException exception) {
        return new ResponseEntity<>(ApiErrorResponse.of(exception), exception.getHttpStatus());
    }

    @ApiResponse(
        responseCode = "400",
        description = "Quando for passado parâmetros e/ou campos inválidos.",
        content = @Content(schema = @Schema(implementation = ValidationErrorResponse.class))
    )
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status,
                                                                  WebRequest request) {
        var errorResponses = ex.getFieldErrors().stream()
            .map(ValidationErrorResponse::of)
            .toList();
        return new ResponseEntity<>(errorResponses, HttpStatus.BAD_REQUEST);
    }

    @ApiResponse(
        responseCode = "400",
        description = "Quando for passado parâmetros e/ou campos inválidos.",
        content = @Content(schema = @Schema(implementation = ValidationErrorResponse.class))
    )
    @ExceptionHandler(ConstraintViolationException.class)
    public ResponseEntity<Object> handleConstraintViolation(ConstraintViolationException ex) {
        var errorResponses = ex.getConstraintViolations().stream()
            .map(ValidationErrorResponse::of)
            .toList();
        return new ResponseEntity<>(errorResponses, HttpStatus.BAD_REQUEST);
    }
}
