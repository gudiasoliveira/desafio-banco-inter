package net.gustavodias.desafioInter.modules.commons.predicates;

import com.querydsl.core.BooleanBuilder;

public class BasePredicate {

    protected final BooleanBuilder predicate = new BooleanBuilder();

    public BooleanBuilder build() {
        return this.predicate;
    }
}
