package net.gustavodias.desafioInter.config;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCacheManager;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ConfigIT {

    @Autowired
    private CacheManager cacheManager;

    @Test
    @DisplayName("Deve usar o gerenciador de cache Caffeine.")
    public void deveUsarOGerenciadorDeCacheCaffeine() {
        assertThat(cacheManager).isInstanceOf(CaffeineCacheManager.class);
    }
}
