package net.gustavodias.desafioInter.modules.investimento.services;

import lombok.experimental.UtilityClass;
import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.investimento.models.Investimento;
import net.gustavodias.desafioInter.modules.investimento.models.InvestimentoEmpresa;

import java.util.Comparator;

import static org.assertj.core.api.Assertions.assertThat;

@UtilityClass
public class InvestimentoServiceTestUtils {

    public static void assertTrocoOtimizado(Investimento investimento) {
        investimento.getInvestimentoEmpresas().stream()
            .map(InvestimentoEmpresa::getAcao)
            .distinct()
            .min(Comparator.comparing(Acao::getPreco))
            .ifPresentOrElse(
                acao -> assertThat(investimento.getTroco()).isLessThan(acao.getPreco()),
                () -> assertThat(investimento.getTroco()).isZero()
            );
    }
}
