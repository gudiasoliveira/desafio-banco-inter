package net.gustavodias.desafioInter.modules.investimento.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

import static net.gustavodias.desafioInter.modules.commons.utils.NumberUtils.roundCurrency;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class Investimento {

    @With
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, scale = 2)
    private Float valorInvestido;

    @OneToMany(cascade = CascadeType.ALL)
    @JoinColumn(
        name = "investimento_id",
        referencedColumnName = "id",
        nullable = false,
        foreignKey = @ForeignKey(name = "fk_invemp_investimento")
    )
    private List<InvestimentoEmpresa> investimentoEmpresas;

    public Float getValorTotal() {
        var valorTotal = investimentoEmpresas.stream().mapToDouble(InvestimentoEmpresa::getValorTotal).sum();
        return roundCurrency((float) valorTotal);
    }

    public Float getTroco() {
        return getValorInvestido() - getValorTotal();
    }
}
