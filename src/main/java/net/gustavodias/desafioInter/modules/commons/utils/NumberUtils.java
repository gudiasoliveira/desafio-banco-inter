package net.gustavodias.desafioInter.modules.commons.utils;

import lombok.experimental.UtilityClass;

@UtilityClass
public class NumberUtils {

    public static float roundCurrency(float value) {
        return Math.round(value * 100) / 100.0f;
    }
}
