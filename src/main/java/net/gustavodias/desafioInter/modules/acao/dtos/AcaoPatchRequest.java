package net.gustavodias.desafioInter.modules.acao.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import net.gustavodias.desafioInter.modules.commons.validators.Currency;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AcaoPatchRequest {

    @NotNull
    @Schema(description = "Ticker da ação que deseja-se editar")
    private String ticker;

    @Currency
    @Positive
    @Schema(description = "Preço da ação para o qual deseja-se atualizar")
    private Float preco;

    @Schema(description = "Estado da ação para o qual deseja-se atualizar")
    private EActiveStatus ativo;
}
