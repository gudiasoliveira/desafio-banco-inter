package net.gustavodias.desafioInter.modules.investimento.controllers;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import net.gustavodias.desafioInter.modules.commons.validators.Currency;
import net.gustavodias.desafioInter.modules.investimento.dtos.InvestimentoResponse;
import net.gustavodias.desafioInter.modules.investimento.dtos.PacoteInvestimentoRequest;
import net.gustavodias.desafioInter.modules.investimento.services.InvestimentoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Positive;

@RestController
@Validated
@RequestMapping("investimento")
public class InvestimentoController {

    @Autowired
    private InvestimentoService service;

    @Operation(
        summary = "Busca todos os investimentos cadastrados"
    )
    @GetMapping
    public Iterable<InvestimentoResponse> getAll() {
        return InvestimentoResponse.listOf(service.getAll());
    }

    @Operation(
        summary = "Simula a criação de um investimento otimizado com um conjunto de ações com diferentes quantidades.",
        description = "Retorna uma simulação de um investimento que seria criado, garantindo a diversificação, homogenização" +
            " das diferentes ações, e garantindo o menor troco possível."
    )
    @GetMapping("pacote")
    public InvestimentoResponse simularPacote(
        @Parameter(description = "Quantidade de ações <strong>distintas</strong> a serem incluídas") @Positive int quantidadeDeAcoes,
        @Parameter(description = "Qual o valor em dinheiro deseja-se aplicar, podendo ter troco") @Positive @Currency float valorAInvestir) {
        return InvestimentoResponse.of(service.generatePackage(quantidadeDeAcoes, valorAInvestir));
    }

    @Operation(
        summary = "Cria um investimento otimizado com um conjunto de ações com diferentes quantidades.",
        description = "O investimento que é criado, garantindo a diversificação, homogenização" +
            " das diferentes ações, e garantindo o menor troco possível."
    )
    @PostMapping("pacote")
    @ResponseStatus(HttpStatus.CREATED)
    public InvestimentoResponse criarPacote(@RequestBody @Valid PacoteInvestimentoRequest request) {
        return InvestimentoResponse.of(service.createPackage(request));
    }
}
