package net.gustavodias.desafioInter.modules.acao.repositories;

import com.querydsl.jpa.impl.JPAQuery;
import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.acao.models.QAcao;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

public class AcaoCustomRepositoryImpl implements AcaoCustomRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<Acao> findMelhores(int quantidadeDeAcoes, float valorAInvestir) {
        return new JPAQuery<Acao>(entityManager)
            .from(QAcao.acao)
            .where(QAcao.acao.ativa.eq(EActiveStatus.A)
                .and(QAcao.acao.preco.loe(valorAInvestir)))
            .orderBy(QAcao.acao.preco.asc())
            .limit(quantidadeDeAcoes)
            .fetch();
    }
}
