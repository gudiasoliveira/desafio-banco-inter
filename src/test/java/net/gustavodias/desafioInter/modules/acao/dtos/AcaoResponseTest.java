package net.gustavodias.desafioInter.modules.acao.dtos;

import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.tuple;

public class AcaoResponseTest {

    @Test
    @DisplayName("getAtivoDescricao." +
        " Ativo e sua descrição nulls." +
        " Quando ativo for null.")
    public void getAtivoDescricao_ativoESuaDescricaoNulls_quandoAtivoForNull() {
        var response = new AcaoResponse();
        assertThat(response)
            .extracting(AcaoResponse::getAtivo, AcaoResponse::getAtivoDescricao)
            .containsOnlyNulls();
    }

    @Test
    @DisplayName("of." +
        " Response com os valores de campo corretos." +
        " Quando o model for válido.")
    public void of_responseComCamposCorretos_quandoModelForValido() {
        var acao = Acao.builder()
            .ticker("MGLU5")
            .nome("Magazine Luíza 5")
            .preco(19.34f)
            .ativa(EActiveStatus.I)
            .build();

        assertThat(AcaoResponse.of(acao))
            .hasFieldOrPropertyWithValue("ticker", "MGLU5")
            .hasFieldOrPropertyWithValue("nome", "Magazine Luíza 5")
            .hasFieldOrPropertyWithValue("preco", 19.34f)
            .hasFieldOrPropertyWithValue("ativo", EActiveStatus.I)
            .hasFieldOrPropertyWithValue("ativoDescricao", "Inativo");
    }

    @Test
    @DisplayName("listOf." +
        " Lista de response com os valores de campo corretos." +
        " Quando os models forem válidos.")
    public void listOf_listaDeResponsesComCamposCorretos_quandoModelsForemValidos() {
        var acoes = List.of(
            Acao.builder()
                .ticker("MGLU24")
                .nome("Magalu")
                .preco(21f)
                .ativa(EActiveStatus.A)
                .build(),
            Acao.builder()
                .ticker("SULA12")
                .nome("Sul-américa")
                .preco(31.01f)
                .ativa(EActiveStatus.I)
                .build()
        );

        var responses = AcaoResponse.listOf(acoes);

        assertThat(responses)
            .extracting("ticker", "nome", "preco")
            .containsExactly(
                tuple("MGLU24", "Magalu", 21f),
                tuple("SULA12", "Sul-américa", 31.01f)
            );
        assertThat(responses)
            .extracting("ativo", "ativoDescricao")
            .containsExactly(
                tuple(EActiveStatus.A, "Ativo"),
                tuple(EActiveStatus.I, "Inativo")
            );
    }

    @Test
    @DisplayName("listOf." +
        " Lista de responses vazia." +
        " Quando a lista de ações for vazia.")
    public void listOf_listaVazia_quandoListaDeAcoesForVazia() {
        assertThat(AcaoResponse.listOf(List.of())).isEmpty();
    }
}
