package net.gustavodias.desafioInter.modules.investimento.services;

import net.gustavodias.desafioInter.modules.acao.services.AcaoService;
import net.gustavodias.desafioInter.modules.investimento.dtos.PacoteInvestimentoRequest;
import net.gustavodias.desafioInter.modules.investimento.models.Investimento;
import net.gustavodias.desafioInter.modules.investimento.models.InvestimentoEmpresa;
import net.gustavodias.desafioInter.modules.investimento.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.concurrent.atomic.AtomicReference;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository repository;
    @Autowired
    private AcaoService acaoService;

    public List<Investimento> getAll() {
        return repository.findAll();
    }

    public Investimento generatePackage(int quantidadeDeAcoes, float valorAInvestir) {
        var acoesAInvestir = acaoService.findMelhores(quantidadeDeAcoes, valorAInvestir);

        var investimentoEmpresas = acoesAInvestir.stream()
            .map((acao -> InvestimentoEmpresa.builder()
                .acao(acao)
                .quantidade(0)
                .build()))
            .toList();

        var restanteAInvestir = new AtomicReference<>(valorAInvestir);
        while (restanteAInvestir.get() >= investimentoEmpresas.get(0).getAcao().getPreco()) {
            investimentoEmpresas.stream()
                .takeWhile(investimentoEmpresa -> restanteAInvestir.get() >= investimentoEmpresa.getAcao().getPreco())
                .forEach(investimentoEmpresa -> {
                    investimentoEmpresa.adicionarAcao();
                    restanteAInvestir.updateAndGet(v -> v - investimentoEmpresa.getAcao().getPreco());
                });
        }

        return Investimento.builder()
            .valorInvestido(valorAInvestir)
            .investimentoEmpresas(investimentoEmpresas.stream().filter(InvestimentoEmpresa::hasAcoes).toList())
            .build();
    }

    public Investimento createPackage(PacoteInvestimentoRequest request) {
        var investimento = generatePackage(request.getQuantidadeDeAcoes(), request.getValorAInvestir());
        return repository.save(investimento);
    }
}
