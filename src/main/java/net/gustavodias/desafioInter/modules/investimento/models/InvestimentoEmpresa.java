package net.gustavodias.desafioInter.modules.investimento.models;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import net.gustavodias.desafioInter.modules.acao.models.Acao;

import javax.persistence.*;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
@Entity
public class InvestimentoEmpresa {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(
        name = "acao_ticker",
        referencedColumnName = "ticker",
        nullable = false,
        foreignKey = @ForeignKey(name = "fk_invemp_acao")
    )
    private Acao acao;

    @Column(nullable = false)
    private Integer quantidade;

    public boolean hasAcoes() {
        return quantidade > 0;
    }

    public void adicionarAcao() {
        quantidade++;
    }

    public Float getValorTotal() {
        return acao.getPreco() * quantidade;
    }
}
