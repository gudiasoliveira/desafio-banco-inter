package net.gustavodias.desafioInter.modules.acao.models;

import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPatchRequest;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPostRequest;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

public class AcaoTest {

    @Test
    @DisplayName("edit." +
        " Deve alterar apenas o preço." +
        " Quando o request tiver apenas o preço.")
    public void edit_deveAlterarApenasOPreco_quandoRequestTiverApenasOPreco() {
        var acao = Acao.builder()
            .ativa(EActiveStatus.A)
            .preco(12.70f)
            .build();

        var request = AcaoPatchRequest.builder()
            .preco(21.45f)
            .build();
        assertThat(request.getAtivo()).isNull();

        acao.edit(request);

        assertThat(acao)
            .hasFieldOrPropertyWithValue("ativa", EActiveStatus.A)
            .hasFieldOrPropertyWithValue("preco", 21.45f);
    }

    @Test
    @DisplayName("edit." +
        " Deve alterar apenas o status ativo." +
        " Quando o request tiver apenas o status ativo.")
    public void edit_deveAlterarApenasOAtivo_quandoRequestTiverApenasOAtivo() {
        var acao = Acao.builder()
            .ativa(EActiveStatus.A)
            .preco(12.70f)
            .build();

        var request = AcaoPatchRequest.builder()
            .ativo(EActiveStatus.I)
            .build();
        assertThat(request.getPreco()).isNull();

        acao.edit(request);

        assertThat(acao)
            .hasFieldOrPropertyWithValue("ativa", EActiveStatus.I)
            .hasFieldOrPropertyWithValue("preco", 12.70f);
    }

    @Test
    @DisplayName("edit." +
        " Deve alterar o preço e o status ativo." +
        " Quando o request tiver o preço e o status ativo.")
    public void edit_deveAlterarPrecoEAtivo_quandoRequestTiverPrecoEAtivo() {
        var acao = Acao.builder()
            .ativa(EActiveStatus.A)
            .preco(12.70f)
            .build();

        var request = AcaoPatchRequest.builder()
            .ativo(EActiveStatus.I)
            .preco(21.45f)
            .build();

        acao.edit(request);

        assertThat(acao)
            .hasFieldOrPropertyWithValue("ativa", EActiveStatus.I)
            .hasFieldOrPropertyWithValue("preco", 21.45f);
    }

    @Test
    @DisplayName("edit." +
        " Deve não alterar nada." +
        " Quando o request não tiver nem o preço nem o status ativo.")
    public void edit_deveNaoAlteraNada_quandoRequestNaoTiverNemPrecoNemAtivo() {
        var acao = Acao.builder()
            .ativa(EActiveStatus.A)
            .preco(12.70f)
            .build();

        var request = new AcaoPatchRequest();
        assertThat(request.getPreco()).isNull();
        assertThat(request.getAtivo()).isNull();

        acao.edit(request);

        assertThat(acao)
            .hasFieldOrPropertyWithValue("ativa", EActiveStatus.A)
            .hasFieldOrPropertyWithValue("preco", 12.70f);
    }

    @Test
    @DisplayName("of." +
        " Model com os campos corretos." +
        " Quando o request for válido.")
    public void of_modelComOsCamposCorretos_quandoORequestForValido() {
        var request = AcaoPostRequest.builder()
            .ticker("AMAR7")
            .nome("Marisa 7")
            .preco(8.20f)
            .ativo(EActiveStatus.I)
            .build();

        assertThat(Acao.of(request))
            .hasFieldOrPropertyWithValue("ticker", "AMAR7")
            .hasFieldOrPropertyWithValue("nome", "Marisa 7")
            .hasFieldOrPropertyWithValue("preco", 8.20f)
            .hasFieldOrPropertyWithValue("ativa", EActiveStatus.I);
    }
}
