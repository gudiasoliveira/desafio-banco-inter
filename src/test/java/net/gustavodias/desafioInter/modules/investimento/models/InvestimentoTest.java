package net.gustavodias.desafioInter.modules.investimento.models;

import net.gustavodias.desafioInter.modules.acao.models.Acao;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.List;
import java.util.stream.Stream;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Offset.offset;
import static org.mockito.Mockito.spy;
import static org.mockito.Mockito.when;

public class InvestimentoTest {

    @Test
    @DisplayName("getValorTotal." +
        " Valor correto." +
        " Quando houverem várias ações.")
    public void getValorTotal_valorCorreto_quandoVariasAcoes() {
        var investimentoEmpresas = umInvestimentoEmpresasComValorTotal(51.20f, 100.00f, 75.45f);
        var investimento = Investimento.builder().investimentoEmpresas(investimentoEmpresas).build();
        assertThat(investimento.getValorTotal()).isCloseTo(226.65f, offset(0.001f));
    }

    @Test
    @DisplayName("getValorTotal." +
        " O mesmo preço da única ação." +
        " Quando houver apenas uma ação.")
    public void getValorTotal_precoDaAcao_quandoUmaAcao() {
        var investimentoEmpresas = umInvestimentoEmpresasComValorTotal(34.12f);
        var investimento = Investimento.builder().investimentoEmpresas(investimentoEmpresas).build();
        assertThat(investimento.getValorTotal()).isCloseTo(34.12f, offset(0.001f));
    }

    @Test
    @DisplayName("getValorTotal." +
        " Zero." +
        " Quando não houver ações.")
    public void getValorTotal_zero_quandoNaoHouverAcoes() {
        var investimento = Investimento.builder().investimentoEmpresas(List.of()).build();
        assertThat(investimento.getValorTotal()).isZero();
    }

    @Test
    @DisplayName("getTroco." +
        " Valor correto." +
        " Quando houver maior valor investido que o total das ações.")
    public void getTroco_valorCorreto_quandoHouverMaiorValorInvestidoQueOTotalDasAcoes() {
        var investimento = spy(Investimento.builder()
            .investimentoEmpresas(List.of())
            .valorInvestido(100f)
            .build());
        when(investimento.getValorTotal()).thenReturn(91.70f);

        assertThat(investimento.getTroco()).isCloseTo(8.30f, offset(0.001f));
    }

    private List<InvestimentoEmpresa> umInvestimentoEmpresasComValorTotal(Float... valoresTotais) {
        return Stream.of(valoresTotais).map(this::umInvestimentoEmpresaComValorTotal).toList();
    }

    private InvestimentoEmpresa umInvestimentoEmpresaComValorTotal(float valorTotal) {
        var investimentoEmpresa = spy(InvestimentoEmpresa.builder()
            .acao(Acao.builder().preco(0f).build())
            .quantidade(0)
            .build());
        when(investimentoEmpresa.getValorTotal()).thenReturn(valorTotal);
        return investimentoEmpresa;
    }
}
