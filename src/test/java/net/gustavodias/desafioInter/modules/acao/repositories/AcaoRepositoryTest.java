package net.gustavodias.desafioInter.modules.acao.repositories;

import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.groups.Tuple.tuple;

@ExtendWith(SpringExtension.class)
@DataJpaTest
@Sql("classpath:test-sql-scripts/acao/acao-repository-test.sql")
public class AcaoRepositoryTest {

    @Autowired
    private AcaoRepository repository;

    @Test
    @DisplayName("findMelhores." +
        " Melhores ações na lógica e ordem correta." +
        " Quando houverem ações disponíveis com o preço.")
    public void findMelhores_melhoresAcoesNaLogicaEOrdemCorreta_quandoHouveremAcoesDisponiveisComPreco() {
        assertThat(repository.findMelhores(4, 100.00f))
            .extracting("ticker", "nome", "preco", "ativa")
            .containsExactly(
                tuple("AMAR3", "Marisa", 6.30f, EActiveStatus.A),
                tuple("MGLU4", "Magazine Luíza 4", 18.80f, EActiveStatus.A),
                tuple("SULA11", "Sulamérica", 28.26f, EActiveStatus.A),
                tuple("LREN3", "Renner", 36.95f, EActiveStatus.A)
            );
    }

    @Test
    @DisplayName("findMelhores." +
        " Melhores ações na lógica e ordem correta com o preço disponível." +
        " Quando houverem nem todas as ações disponíveis com o preço.")
    public void findMelhores_melhoresAcoesNaLogicaEOrdemCorretaComPrecoDisponivel_quandoHouveremNemTodasAcoesDisponiveisComPreco() {
        assertThat(repository.findMelhores(5, 30.00f))
            .extracting("ticker", "nome", "preco", "ativa")
            .containsExactly(
                tuple("AMAR3", "Marisa", 6.30f, EActiveStatus.A),
                tuple("MGLU4", "Magazine Luíza 4", 18.80f, EActiveStatus.A),
                tuple("SULA11", "Sulamérica", 28.26f, EActiveStatus.A)
            );
    }
}
