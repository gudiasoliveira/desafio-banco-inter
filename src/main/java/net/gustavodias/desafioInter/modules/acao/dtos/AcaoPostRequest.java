package net.gustavodias.desafioInter.modules.acao.dtos;

import lombok.Builder;
import lombok.Data;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import net.gustavodias.desafioInter.modules.commons.validators.Currency;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

@Data
@Builder
public class AcaoPostRequest {

    @NotBlank
    private String ticker;

    @NotBlank
    private String nome;

    @NotNull
    @Currency
    @Positive
    private Float preco;

    @NotNull
    @Builder.Default
    private EActiveStatus ativo = EActiveStatus.A;
}
