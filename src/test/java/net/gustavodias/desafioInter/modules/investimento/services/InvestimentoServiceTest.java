package net.gustavodias.desafioInter.modules.investimento.services;

import net.gustavodias.desafioInter.modules.acao.models.Acao;
import net.gustavodias.desafioInter.modules.acao.services.AcaoService;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import net.gustavodias.desafioInter.modules.investimento.dtos.PacoteInvestimentoRequest;
import net.gustavodias.desafioInter.modules.investimento.models.Investimento;
import net.gustavodias.desafioInter.modules.investimento.repositories.InvestimentoRepository;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Comparator;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Stream;

import static net.gustavodias.desafioInter.modules.investimento.services.InvestimentoServiceTestUtils.assertTrocoOtimizado;
import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.data.Offset.offset;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class InvestimentoServiceTest {

    @InjectMocks
    private InvestimentoService service;

    @Mock
    private InvestimentoRepository repository;
    @Mock
    private AcaoService acaoService;

    @Test
    @DisplayName("generatePackage." +
        " Deve retornar um investimento cujo troco seja mais barato que a ação mais barata." +
        " Quando houver várias ações.")
    public void generatePackage_deveRetornarInvestimentoComTrocoMenorQueAcaoMaisBarata_quandoVariasAcoes() {
        when(acaoService.findMelhores(4, 2100.00f))
            .thenReturn(umaAcoesComPrecoList(22.47f, 8.63f, 102.14f, 30.00f));
        assertTrocoOtimizado(service.generatePackage(4, 2100.00f));

        when(acaoService.findMelhores(3, 231.10f))
            .thenReturn(umaAcoesComPrecoList(21.45f, 6.13f, 32.00f));
        assertTrocoOtimizado(service.generatePackage(3, 231.10f));

        when(acaoService.findMelhores(7, 3000))
            .thenReturn(umaAcoesComPrecoList(22.47f, 8.63f, 32.00f, 102.14f, 6.13f, 30.00f, 50.00f));
        assertTrocoOtimizado(service.generatePackage(7, 3000));
    }

    @Test
    @DisplayName("generatePackage." +
        " Deve retornar um investimento cujo troco seja mais barato que a ação." +
        " Quando houver apenas uma ação.")
    public void generatePackage_deveRetornarInvestimentoComTrocoMenorQueAcao_quandoApenasUmaAcao() {
        when(acaoService.findMelhores(1, 500))
            .thenReturn(umaAcoesComPrecoList(10.40f));

        var investimento = service.generatePackage(1, 500);

        assertTrocoOtimizado(investimento);
        assertThat(investimento.getTroco()).isCloseTo(0.80f, offset(0.001f));
    }

    @Test
    @DisplayName("createPackage." +
        " Deve criar e retornar um investimento cujo troco seja mais barato que a ação mais barata." +
        " Quando houver várias ações.")
    public void createPackage_deveCriarInvestimentoComTrocoMenorQueAcaoMaisBarata_quandoVariasAcoes() {
        var random = new Random(1233);

        when(repository.save(any())).thenAnswer(invocationOnMock -> {
            Investimento investimento = invocationOnMock.getArgument(0);
            assertThat(investimento.getId()).isNull();
            return investimento.withId(random.nextLong());
        });

        when(acaoService.findMelhores(4, 2100.00f))
            .thenReturn(umaAcoesComPrecoList(22.47f, 8.63f, 102.14f, 30.00f));
        var investimento = service.createPackage(PacoteInvestimentoRequest.builder()
            .quantidadeDeAcoes(4)
            .valorAInvestir(2100.00f)
            .build());
        assertThat(investimento.getId()).isNotNull();
        assertTrocoOtimizado(investimento);

        when(acaoService.findMelhores(3, 231.10f))
            .thenReturn(umaAcoesComPrecoList(21.45f, 6.13f, 32.00f));
        investimento = service.createPackage(PacoteInvestimentoRequest.builder()
            .quantidadeDeAcoes(3)
            .valorAInvestir(231.10f)
            .build());
        assertThat(investimento.getId()).isNotNull();
        assertTrocoOtimizado(investimento);

        when(acaoService.findMelhores(7, 3000))
            .thenReturn(umaAcoesComPrecoList(22.47f, 8.63f, 32.00f, 102.14f, 6.13f, 30.00f, 50.00f));
        investimento = service.createPackage(PacoteInvestimentoRequest.builder()
            .quantidadeDeAcoes(7)
            .valorAInvestir(3000.00f)
            .build());
        assertThat(investimento.getId()).isNotNull();
        assertTrocoOtimizado(investimento);
    }

    @Test
    @DisplayName("createPackage." +
        " Deve criar e retornar um investimento cujo troco seja mais barato que a ação." +
        " Quando houver apenas uma ação.")
    public void createPackage_deveCriarInvestimentoComTrocoMenorQueAcao_quandoApenasUmaAcao() {
        var random = new Random(336);

        when(repository.save(any())).thenAnswer(invocationOnMock -> {
            Investimento investimento = invocationOnMock.getArgument(0);
            assertThat(investimento.getId()).isNull();
            return investimento.withId(random.nextLong());
        });

        when(acaoService.findMelhores(1, 500))
            .thenReturn(umaAcoesComPrecoList(10.40f));

        var investimento = service.createPackage(PacoteInvestimentoRequest.builder()
            .quantidadeDeAcoes(1)
            .valorAInvestir(500.00f)
            .build());
        assertThat(investimento.getId()).isNotNull();
        assertTrocoOtimizado(investimento);

        assertThat(investimento.getTroco()).isCloseTo(0.80f, offset(0.001f));
    }

    private List<Acao> umaAcoesComPrecoList(Float... precos) {
        var tickerSuffixCounter = new AtomicInteger(1);
        return Stream.of(precos)
            .map(preco -> umaAcaoBuilder(tickerSuffixCounter).preco(preco).build())
            .sorted(Comparator.comparing(Acao::getPreco))
            .toList();
    }

    private Acao.AcaoBuilder umaAcaoBuilder(AtomicInteger tickerSuffixCounter) {
        var currentCounter = tickerSuffixCounter.getAndIncrement();

        return Acao.builder()
            .ticker("ACEMT" + currentCounter)
            .ativa(EActiveStatus.A)
            .nome("Ação da empresa de teste " + currentCounter);
    }
}
