package net.gustavodias.desafioInter.modules.investimento.services;

import net.gustavodias.desafioInter.modules.acao.repositories.AcaoRepository;
import net.gustavodias.desafioInter.modules.investimento.dtos.PacoteInvestimentoRequest;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static net.gustavodias.desafioInter.modules.investimento.services.InvestimentoServiceTestUtils.assertTrocoOtimizado;
import static org.assertj.core.api.Assertions.assertThat;

@ExtendWith(SpringExtension.class)
@SpringBootTest
@Sql("classpath:test-sql-scripts/investimento/investimento-service-it.sql")
public class InvestimentoServiceIT {

    @Autowired
    private InvestimentoService service;
    @Autowired
    private AcaoRepository acaoRepository;

    @BeforeEach
    public void beforeEach() {
        assertThat(acaoRepository.count()).isNotZero();
    }

    @AfterEach
    public void afterEach() {
        assertThat(acaoRepository.count()).isNotZero();
    }

    @Test
    @DisplayName("generatePackage." +
        " Deve retornar um investimento cujo troco seja mais barato que a ação mais barata." +
        " Quando houver várias ações.")
    public void generatePackage_deveRetornarInvestimentoComTrocoMenorQueAcaoMaisBarata_quandoVariasAcoes() {
        assertTrocoOtimizado(service.generatePackage(4, 2100.00f));
        assertTrocoOtimizado(service.generatePackage(3, 231.10f));
        assertTrocoOtimizado(service.generatePackage(7, 3000));
    }

    @Test
    @DisplayName("generatePackage." +
        " Deve retornar um investimento cujo troco seja mais barato que a ação." +
        " Quando houver apenas uma ação.")
    public void generatePackage_deveRetornarInvestimentoComTrocoMenorQueAcao_quandoApenasUmaAcao() {
        assertTrocoOtimizado(service.generatePackage(1, 500));
    }

    @Test
    @DisplayName("createPackage." +
        " Deve criar e retornar um investimento cujo troco seja mais barato que a ação mais barata." +
        " Quando houver várias ações.")
    public void createPackage_deveCriarInvestimentoComTrocoMenorQueAcaoMaisBarata_quandoVariasAcoes() {
        var investimento = service.createPackage(PacoteInvestimentoRequest.builder()
            .quantidadeDeAcoes(4)
            .valorAInvestir(2100.00f)
            .build());
        assertThat(investimento.getId()).isNotNull();
        assertTrocoOtimizado(investimento);
    }
}
