package net.gustavodias.desafioInter.modules.investimento.dtos;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.Builder;
import lombok.Data;
import net.gustavodias.desafioInter.modules.investimento.mapper.InvestimentoMapper;
import net.gustavodias.desafioInter.modules.investimento.models.Investimento;

import java.util.List;

import static net.gustavodias.desafioInter.modules.commons.utils.NumberUtils.roundCurrency;

@Data
@Builder
public class InvestimentoResponse {

    private Long id;

    @Schema(description = "Número de investimentos, cada um com a ação e sua quantidade comprada")
    private List<InvestimentoEmpresaResponse> investimentoEmpresas;

    private Float valorInvestido;

    @Schema(description = "Preço total somando todas as ações compradas, com suas respectivas quantidades.")
    public Float getValorTotalAcoes() {
        var valor = investimentoEmpresas.stream()
            .mapToDouble(InvestimentoEmpresaResponse::getValorTotal)
            .sum();
        return roundCurrency((float) valor);
    }

    @Schema(description = "Quantidade de ações diferentes compradas")
    public Integer getQuantidadeDeAcoesDistintas() {
        return investimentoEmpresas.size();
    }

    @Schema(description = "Pode ocorrer que o valor investido não caber totalmente no valor total das ações." +
        " Por este motivo, é devolvido um troco ao investidor (<code>valorInvestido - valorTotalAcoes</code>)")
    public Float getTroco() {
        return roundCurrency(valorInvestido - getValorTotalAcoes());
    }

    public static InvestimentoResponse of(Investimento investimento) {
        return InvestimentoMapper.INSTANCE.modelToResponse(investimento);
    }

    public static List<InvestimentoResponse> listOf(Iterable<Investimento> investimentos) {
        return InvestimentoMapper.INSTANCE.modelToResponseList(investimentos);
    }
}
