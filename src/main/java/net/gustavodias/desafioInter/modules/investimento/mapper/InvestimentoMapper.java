package net.gustavodias.desafioInter.modules.investimento.mapper;

import net.gustavodias.desafioInter.modules.acao.mappers.AcaoMapper;
import net.gustavodias.desafioInter.modules.investimento.dtos.InvestimentoResponse;
import net.gustavodias.desafioInter.modules.investimento.models.Investimento;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

import java.util.List;

@Mapper(uses = { AcaoMapper.class })
public interface InvestimentoMapper {

    InvestimentoMapper INSTANCE = Mappers.getMapper(InvestimentoMapper.class);

    InvestimentoResponse modelToResponse(Investimento investimento);

    List<InvestimentoResponse> modelToResponseList(Iterable<Investimento> investimentos);
}
