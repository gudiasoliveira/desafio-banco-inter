package net.gustavodias.desafioInter.modules.acao.models;

import lombok.*;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPatchRequest;
import net.gustavodias.desafioInter.modules.acao.dtos.AcaoPostRequest;
import net.gustavodias.desafioInter.modules.acao.mappers.AcaoMapper;
import net.gustavodias.desafioInter.modules.commons.enums.EActiveStatus;
import net.gustavodias.desafioInter.modules.commons.validators.Currency;
import org.hibernate.validator.constraints.Length;

import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;
import java.util.Optional;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "ticker")
@Entity
public class Acao {

    @Id
    @Length(max = 10)
    private String ticker;

    @NotBlank
    @Column(nullable = false, length = 120)
    private String nome;

    @Currency
    @Positive
    @Column(nullable = false, scale = 2)
    private Float preco;

    @Column(nullable = false)
    @Enumerated(EnumType.STRING)
    private EActiveStatus ativa;

    public void edit(AcaoPatchRequest request) {
        Optional.ofNullable(request.getPreco()).ifPresent(this::setPreco);
        Optional.ofNullable(request.getAtivo()).ifPresent(this::setAtiva);
    }

    public static Acao of(AcaoPostRequest request) {
        return AcaoMapper.INSTANCE.postRequestToModel(request);
    }
}
